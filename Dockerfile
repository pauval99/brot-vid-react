FROM node:14.14

ENV PATH /app/node_modules/.bin:$PATH

COPY ./app /app

WORKDIR /app

RUN npm install

CMD ["npm", "start"]