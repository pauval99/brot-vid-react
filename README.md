## Develop

### With Docker

```sh
sudo apt install docker docker-compose
docker-compose up --build //localhost:8080
```

### With Npm

```sh
cd app/
npm install
npm start //localhost:8080
```

## Users

 + **email:** admin@admin.com
 + **password:** admin
 
 + **email:** alumno@alumno.com
 + **password:** alumno
 
 + **email:** profesor@profesor.com
 + **password:** profesor

## Web URL
+ [http://35.180.26.95/](http://35.180.26.95/) Down

## API Doc
+ [Swagger](https://app.swaggerhub.com/apis-docs/dardaswagger/Brot-Vid/1.0.0)

## SonarCloud
+ [SonarCloud](https://sonarcloud.io/dashboard?id=pauval99_brot-vid-react)

## Colors
 + lightblue: 0x00caf2
 + darkblue: 0x003951
 + email & password background: 0xEDF5F7

## Twitter

+ **User:** [@Leonard88831201](https://twitter.com/Leonard88831201)