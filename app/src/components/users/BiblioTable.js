export default function BiblioTable(props) {
	return (
		<div className="row m-3">
			<h4 className="my-2">Historial de Asientos de la Biblioteca</h4>
			<table className="table table-bordered">
				<thead>
					<tr>
						<th scope="col bg-white">Fecha</th>
						<th scope="col">Intervalo</th>
						<th scope="col">Piso</th>
						<th scope="col">Silla</th>
					</tr>
				</thead>
				<tbody>
					{ props.seats.map((seat) => {
							return (
								<tr key={seat._id}>
									<td>{ seat.date }</td>
									<td>{ seat.h_ini + ' - ' + seat.h_end }</td>
									<td>{ seat.chair.room.floor }</td>
									<td>{ seat.chair.room.number }</td>
								</tr>
							);
						}) 
					}
				</tbody>
			</table>
		</div>
	);
}