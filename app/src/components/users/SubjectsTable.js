export default function SeatTable(props) {
	return (
		<div className="row m-3">
      		<h4 className="my-2">Asignaturas Matriculadas</h4>
			<table className="table table-bordered">
				<thead>
					<tr>
						<th scope="col">Asignaturas</th>
					</tr>
				</thead>
				<tbody>
					{ props.subjects.map((subject) => {
							return (
								<tr key={subject.name}>
									<td>{ subject.name }</td>
								</tr>
							);
						}) 
					}
				</tbody>
			</table>
		</div>
	);
}