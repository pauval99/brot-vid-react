import StringHelper from "../../helpers/StringHelper";
import Moment from 'moment';

export default function SeatTable(props) {
	return (
		<div className="row m-3">
      		<h4 className="my-2">Historial de Asientos de Clase</h4>
			<table className="table table-bordered">
				<thead>
					<tr>
						<th scope="col bg-white">Fecha</th>
						<th scope="col">Aula</th>
						<th scope="col">Silla</th>
						<th scope="col">Assignatura</th>
						<th scope="col">Horario</th>
						<th scope="col">Profesor</th>
					</tr>
				</thead>
				<tbody>
					{ props.seats.map((seat) => {
							return (
								<tr key={seat._id}>
									<td>{ Moment(seat.date).format('DD/MM/Y') }</td>
									<td>{ StringHelper.roomToString(seat.class.room) }</td>
									<td>{ seat.chair.QR[0] }</td>
									<td>{ seat.class.subject.name }</td>
									<td>{ seat.class.schedule.h_ini + ':00 - ' + seat.class.schedule.h_end + ':00' }</td>
									<td>{ seat.class.teacher.displayName }</td>
								</tr>
							);
						}) 
					}
				</tbody>
			</table>
		</div>
	);
}