import React, { Component } from "react";
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";
import RoomsService from '../services/RoomsService';
import Input from "react-validation/build/input";
import NotificationsService from "../services/NotificationsService";
import StringHelper from "../helpers/StringHelper";
import TwitterService from "../services/TwitterService";
import SchoolService from "../services/SchoolService";
import 'moment/locale/es';
import Moment from 'moment';

export default class CreateNotification extends Component {
  constructor(props) {
    super(props);
    this.handleCreation = this.handleCreation.bind(this);
    this.onChangeType = this.onChangeType.bind(this);
    this.onChangeRoom = this.onChangeRoom.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeTweet = this.onChangeTweet.bind(this);

    this.state = {
      openedSchool: true,
      type: 'openRoom',
      rooms: [],
      filteredRooms: [],
      selectedRoom: '',
      title: '',
      description: '',
      tweet: false,
      loading: false,
      message: ""
    };
  }

  componentDidMount() {
    RoomsService.getRooms().then(
      response => {
        this.setState({
          rooms: response.data.message.filter((notification) => notification.building !== 'BIBLIOTECA'),
        });
        this.updateFilteredRooms(this.state.type);
      }
    );

    SchoolService.getSchool().then(
      response => {
        this.setState({
          openedSchool: response.data.message.opened,
        });
      }
    );
  }

  updateFilteredRooms(type) {
    let filteredRooms = [];
    if(type === 'openRoom')
      filteredRooms = this.state.rooms.filter((rooms) => rooms.closed);
    if(type === 'closeRoom') 
      filteredRooms = this.state.rooms.filter((rooms) => !rooms.closed);
    
    this.setState({
      type: type,
      filteredRooms: filteredRooms,
      selectedRoom: filteredRooms[0]
    });
  }

  onChangeType(e) {
    this.updateFilteredRooms(e.target.value);
  }

  onChangeTweet(e) {
    this.setState({
      tweet: e.target.checked
    });
  }

  onChangeRoom(e) {
    this.setState({
      selectedRoom: JSON.parse(e.target.value)
    });
  }

  onChangeTitle(e) {
    this.setState({
      title: e.target.value
    });
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    });
  }

  setCreated() {
    this.setState({
      loading: false,
      message: 'Notificación creada.'
    });
  }

  setError() {
    this.setState({
      loading: false,
      message: 'Error en la creación.'
    });
  }

  editRoom() {
    RoomsService.editRoom(this.state.type, this.state.selectedRoom).then(
      () => {
        this.setCreated();
        this.componentDidMount();
      },
      error => this.setError()
    );
  }

  editSchool() {
    SchoolService.editSchool(this.state.type).then(
      () => {
        this.setCreated()
        this.setState({
          openedSchool: !this.state.openedSchool,
          type: this.state.type === 'openSchool' ? 'closeSchool' : 'openSchool' 
        });
      },
      error => {
        this.setError()
      }
    );
  }

  createNews() {
    NotificationsService.createNews(this.state.title, this.state.description).then(
      () => {
        this.setCreated()
      },
      error => {
        this.setError()
      }
    );
  }

  createSecurityMeasure() {
    NotificationsService.createSecurityMeasure(this.state.title, this.state.description).then(
      () => {
        this.setCreated()
      },
      error => {
        this.setError()
      }
    );
  }

  createRegulation() {
    NotificationsService.createRegulation(this.state.title, this.state.description).then(
      () => {
        this.setCreated()
      },
      error => {
        this.setError()
      }
    );
  }

  handleCreation(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      let status = this.state.title;
      if(this.state.type === 'openRoom' || this.state.type === 'closeRoom') {
        this.editRoom();
        
        let room = StringHelper.roomToString(this.state.selectedRoom);
        if(this.state.type === 'openRoom') status = 'Se ha abierto el aula ' + room + '.';
        else status = 'Se ha cerrado el aula ' + room + '.';
      
      } else if(this.state.type === 'addNews') {
        this.createNews();
      } else if(this.state.type === 'addSecurityMeasures') {
        this.createSecurityMeasure();
      } else if(this.state.type === 'openSchool' || this.state.type === 'closeSchool') {
        this.editSchool();

        if(this.state.type === 'openSchool') status = 'Centro FIB abierto.';
        else status = 'Centro FIB cerrado.';

      } else if(this.state.type === 'addRegulations') {
        this.createRegulation();
      }

      if(this.state.tweet) {
        TwitterService.tweet(Moment().format('[[]HH:mm DD/MM/yyyy[]] ') + status);
        this.setState({ tweet: false });
      }
    } else {
      this.setState({
        loading: false
      });
    }
  }

  render() {
    const { filteredRooms, type, loading, message } = this.state;

    return (
      <div className="col-md-12">
        <div className="card card-container">
          <h3>Crear Notificación</h3>
          <Form onSubmit={ this.handleCreation } ref={ c => { this.form = c; } } >
            
            <div className="form-group">
              <label htmlFor="type">Tipo</label>
              <select className="form-control" name="type" value={type} onChange={this.onChangeType}>
                <option value="openRoom">Apertura Aula</option>
                <option value="closeRoom">Cierre Aula</option>
                { !this.state.openedSchool && <option value="openSchool">Apertura Centro</option>}
                { this.state.openedSchool && <option value="closeSchool">Cerrar Centro</option> }
                <option value="addNews">Nueva Noticia</option>
                <option value="addRegulations">Nueva Regulación</option>
                <option value="addSecurityMeasures">Nueva Medida de Seguridad</option>
              </select>
            </div>
            
            {(type === 'openRoom' || type === 'closeRoom') &&
              <div className="form-group">
                <label htmlFor="type">Aula</label>
                <select className="form-control" name="room" onChange={this.onChangeRoom}>
                { filteredRooms.map((room) => {
                  return (
                    <option key={room._id} value={JSON.stringify(room)}>{StringHelper.roomToString(room)}</option>
                  );
                })}
                </select>
              </div>
            }

            {(type === 'addNews' || type === 'addSecurityMeasures' || type === 'addRegulations') &&
              <>
                <div className="form-group">
                  <label htmlFor="title">Título</label>
                  <Input
                    type="text"
                    className="form-control"
                    name="title"
                    value={this.state.title}
                    onChange={this.onChangeTitle}
                    required
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="description">Descripción</label>
                  <textarea
                    rows='3'
                    className="form-control"
                    name="description"
                    value={this.state.description}
                    onChange={this.onChangeDescription}
                    required
                  />
                </div>
              </>
            }

            <div className="form-group">
              <div className="form-check">
                <input
                  name="tweet"
                  type="checkbox"
                  className="form-check-input"
                  checked={this.state.tweet}
                  onChange={this.onChangeTweet} 
                />
                <label htmlFor="tweet">
                  Tuitear notificación
                </label>
              </div>
            </div>

            <div className="form-group">
              <button
                className="btn btn-primary btn-block"
                disabled={loading}
              >
                {loading && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}
                <span>Crear</span>
              </button>
            </div>

            { message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  { message }
                </div>
              </div>
            )}

            <CheckButton
              style={{ display: "none" }}
              ref={c => {
                this.checkBtn = c;
              }}
            />
          </Form>
        </div>
      </div>
    );
  }
}
