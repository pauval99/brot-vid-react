import React, { Component } from "react";
import UsersService from "../services/UsersService";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChalkboardTeacher, faBookReader, faCalendarAlt } from '@fortawesome/free-solid-svg-icons'
import SeatTable from "./users/SeatTable";
import BiblioTable from "./users/BiblioTable";
import SubjectsTable from "./users/SubjectsTable";

const subjectsTable = 'subjects';
const classTable = 'class';
const biblioTable = 'biblio';

export default class Students extends Component {
  constructor(props) {
    super(props);
    this.onChangeDisplayName = this.onChangeDisplayName.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);

    this.state = {
      students: [],
      filteredStudents: [],
      selectedStudent: {},
      name: '',
      email: '',
      seatHistory: [],
      table: null,
      isLoaded: false
    };
  }

  onChangeDisplayName(e) {
    this.setState({
      filteredStudents: this.state.students.filter(
        (students) => students.displayName.toLowerCase().includes(e.target.value.toLowerCase())),
      name: e.target.value
    });
  }

  onChangeEmail(e) {
    this.setState({
      filteredStudents: this.state.students.filter(
        (students) => students.email.toLowerCase().includes(e.target.value.toLowerCase())),
      email: e.target.value
    });
  }

  componentDidMount() {
    UsersService.getUsers().then(
      response => {
        let student = response.data.message.filter((users) => users.role === 'ALUMNO');
        this.setState({
          students: student,
          filteredStudents: student,
          isLoaded: true
        });
      }
    );
  }

  select(student) {
    UsersService.getSeatHistory(student).then(
      response => {
        this.setState({
          seatHistory: response.data.sort((a, b) => new Date(b.creationDate).getTime() - new Date(a.creationDate).getTime()),
          selectedStudent: student,
          table: classTable
        });
    });
  }

  selectBiblio(student) {
    UsersService.getSeatHistoryBiblio(student).then(
      response => {
        this.setState({
          seatHistory: response.data.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()),
          selectedStudent: student,
          table: biblioTable
        });
    });
  }

  selectSubjects(student) {
    UsersService.getSubjects(student).then(
      response => {
        this.setState({
          subjects: response.data.subjs,
          selectedStudent: student,
          table: subjectsTable
        });
    });
  }

  unselect() {
    this.setState({
      seatHistory: [],
      selectedStudent: {},
      subjects: [],
      table: null
    });
  }

  render() {
    const { seatHistory, filteredStudents, isLoaded, selectedStudent, table, subjects } = this.state;
    return (
      <div className="container">
        <h2 className="mt-2" align="center">Alumnos</h2>
        <div className="row mb-3">
          <div className="col">
            <div className="row mt-2">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChangeDisplayName}
                  placeholder="Buscar por nombre"
                />
              </div>
              <div className="col-7">
                <input
                  type="text"
                  className="form-control"
                  name="email"
                  value={this.state.email}
                  onChange={this.onChangeEmail}
                  placeholder="Buscar por email"
                />              
              </div>
            </div>
            { isLoaded && 
              filteredStudents.map((student) => {
                return (
                  <div key={student.email} className={`mt-2 p-2 rounded ${selectedStudent === student ? 'bg-selected' : 'bg-white'}`} >
                    <div className="row">
                      <div className="notificationArrow ml-3" title="Historial de Clase" onClick={() => (selectedStudent === student && table === classTable) ? this.unselect() : this.select(student) }>
                        <FontAwesomeIcon icon={faChalkboardTeacher} />
                      </div>
                      <div className="notificationArrow ml-3" title="Historial de Biblioteca" onClick={() => (selectedStudent === student && table === biblioTable) ? this.unselect() : this.selectBiblio(student) }>
                        <FontAwesomeIcon icon={faBookReader} />
                      </div>
                      <div className="notificationArrow ml-3" title="Asignaturas Matriculadas" onClick={() => (selectedStudent === student && table === subjectsTable) ? this.unselect() : this.selectSubjects(student) }>
                        <FontAwesomeIcon icon={faCalendarAlt} />
                      </div>
                      <div className="col">
                        <b>{ student.displayName }</b>
                      </div>
                      <div className="col-7">
                        { student.email }
                      </div>
                    </div>
                    { selectedStudent === student && 
                        {
                          class: <SeatTable seats={seatHistory} />,
                          biblio: <BiblioTable seats={seatHistory} />,
                          subjects: <SubjectsTable subjects={subjects} />
                        }[table]
                    }
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    );
  }
}
