import React, { Component } from "react";
import Moment from 'moment';
import 'moment/locale/es';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckSquare } from '@fortawesome/free-solid-svg-icons'

import NotificationsService from '../services/NotificationsService';
import IncidenceService from "../services/IncidenceService";

const notificationTypes = [
  {
    type: 'GENERAL',
    color: 'gold',
    displayName: 'General'
  }, {
    type: 'INCIDENCIA',
    color: 'hotpink',
    displayName: 'Incidencias'
  }, {
    type: 'POSITIVO',
    color: 'deepskyblue',
    displayName: 'Positivo'
  }, {
    type: 'NEGATIVO',
    color: 'mediumorchid',
    displayName: 'Negativo'
  }, {
    type: 'CONFINADO',
    color: 'red',
    displayName: 'Confinado'
  }, {
    type: 'DESCONFINADO',
    color: 'green',
    displayName: 'Desconfinado'
  }, {
    type: 'NOTICIAS',
    color: 'sienna',
    displayName: 'Noticias'
  }, {
    type: 'SEGURIDAD',
    color: 'coral',
    displayName: 'Medidas de Seguridad'
  }, {
    type: 'ASISTENCIA',
    color: 'darkkhaki',
    displayName: 'Asistencia'
  }, {
    type: 'GUIA',
    color: 'turquoise',
    displayName: 'Cambios en la Guia Docente'
  }, {
    type: 'NORMATIVA',
    color: 'limegreen',
    displayName: 'Cambios de Normativa'
  }, {
    type: 'TODAS',
    color: 'grey',
    displayName: 'Todas'
  }
];

export default class Notifications extends Component {
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);

    this.state = {
      notifications: [],
      filteredNotifications: [],
      selectedNotification: [],
      title: '',
      isLoaded: false
    };
  }

  componentDidMount() {
    NotificationsService.getNotifications().then(
      response => {
        let notifications = response.data.sort((a, b) => new Date(b.creationDate).getTime() - new Date(a.creationDate).getTime());
        this.setState({
          notifications: notifications,
          filteredNotifications: notifications,
          isLoaded: true
        });
      }
    );
  }

  resolveIncidence(incidence) {
    IncidenceService.resolveIncidence(incidence).then(
      () => this.setState({
        notifications: this.state.notifications.filter((notification) => notification !== incidence),
        filteredNotifications: this.state.filteredNotifications.filter((notification) => notification !== incidence),
        selectedNotification: this.state.selectedNotification === incidence ? [] : this.state.selectedNotification,
      })
    );
  }

  filterBy(criteria) {
    if(criteria === 'TODAS') 
      this.setState({
        filteredNotifications: this.state.notifications
      });
    else
      this.setState({
        filteredNotifications: this.state.notifications.filter((notifications) => notifications.type === criteria)
      });
  }

  select(notification) {
    if (notification !== this.state.selectedNotification)
      this.setState({
        selectedNotification: notification
      });
  }

  unselect() {
    this.setState({
      selectedNotification: []
    });
  }

  onChangeTitle(e) {
    this.setState({
      filteredNotifications: this.state.notifications.filter(
        (notifications) => notifications.title.toLowerCase().includes(e.target.value.toLowerCase())),
      title: e.target.value
    });
  }

  render() {
    const { filteredNotifications, isLoaded, selectedNotification } = this.state;
    Moment.locale('es');
    return (
      <div className="container">
        <h2 className="mt-2" align="center">Notificaciones</h2>
        <div className="row mb-3" style={{minHeight: '500px'}}>
          <div className="col">
            <div className="filter">
              <div className="row px-2">
                <a href="/notifications/create" className="btn btn-primary btn-block">
                  <span>Crear Notificación</span>
                </a>
              </div>
              <div className="row mt-3">
                <div className="ml-3 mb-0"><u>Filtro</u></div>
              </div>
              { notificationTypes.map((notification) => {
                  return (
                    <div key={notification.type} className="row mt-2">
                      <div className="rounded" style={{ backgroundColor: notification.color, width: '5px' }}></div>
                      <div className="ml-2 clickable" onClick={() => this.filterBy(notification.type)}>{ notification.displayName }</div>
                    </div>
                  );
                })
              }
            </div>
          </div>
          <div className="col-9">
            <div className="row">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  name="title"
                  value={this.state.title}
                  onChange={this.onChangeTitle}
                  placeholder="Buscar por título"
                />
              </div>
            </div>
            { isLoaded && 
              filteredNotifications.map((notification) => {
                return (
                  <div key={notification._id} className={`mt-2 p-2 rounded ${selectedNotification === notification ? 'bg-selected' : 'bg-white'}`} >
                    <div className="row">
                      <div className="rounded ml-2" style={{ backgroundColor: notificationTypes.find(n => n.type === notification.type).color, width: '5px' }}></div>
                      <div className="notificationArrow ml-3" title="Mas Información" onClick={() => selectedNotification === notification ? this.unselect() : this.select(notification) }>
                        { selectedNotification === notification ? "▲" : "▼" }
                      </div>
                      <div className="col-8 pl-1">
                        <b>{ notification.title }</b>
                      </div>
                      { notification.type === 'INCIDENCIA' &&
                        <div 
                          className="clickable"
                          title="Resolver Incidencia"
                          onClick={() =>
                            window.confirm('¿Seguro que quieres resolver la incidencia?') &&
                            this.resolveIncidence(notification)
                          }
                        >
                          <FontAwesomeIcon icon={faCheckSquare} />
                        </div>
                      }
                      <div className="col" align="right" style={{fontSize: '0.9rem'}}>
                        { selectedNotification === notification ? Moment(selectedNotification.creationDate).format('HH:mm DD/MM/yyyy ') : Moment(notification.creationDate).fromNow() }
                      </div>
                    </div>
                    { selectedNotification === notification &&
                      <div className="row m-2">
                        <div className="col">
                          { selectedNotification.description }
                        </div>
                      </div>
                    }
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    );
  }
}
