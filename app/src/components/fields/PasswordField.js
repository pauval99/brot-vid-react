import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faEyeSlash, faLock } from '@fortawesome/free-solid-svg-icons'
import Input from "react-validation/build/input";

export default class PasswordField extends Component {
  constructor(props) {
    super(props);
    this.togglePassword = this.togglePassword.bind(this);

    this.state = {
        isRevealedPassword: false,
    };
  }

  togglePassword() {
    this.setState({
      isRevealedPassword: !this.state.isRevealedPassword
    });
  }

  render() {
    const { className, value, onChange, validations } = this.props
    const { isRevealedPassword } = this.state;

    return (
      <>
        <label htmlFor="password">Contraseña <FontAwesomeIcon icon={faLock} color="#003951" /></label>
        <span onClick={this.togglePassword} className="toggleIcon" title="Mostrar Contraseña">
          {isRevealedPassword ? <FontAwesomeIcon icon={faEyeSlash} /> : <FontAwesomeIcon icon={faEye} />}
        </span>
        <Input
          type={isRevealedPassword ? "text" : "password"}
          className={className}
          name="password"
          value={value}
          onChange={onChange}
          validations={validations}
        />
      </>
    )
  }
}