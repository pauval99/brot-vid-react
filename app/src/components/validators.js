import React from "react";
import validator from 'validator';

export const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Este campo es obligatorio.
      </div>
    );
  }
};
  
export const email = value => {
  if (!validator.isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
          Email no valido.
      </div>
    )
  }
};