import CurrentUser from "../models/CurrentUser";
import APIService from "./APIService";

class RoomsService {
    getRooms() {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        return APIService.get('/rooms', headers);
    }

    editRoom(type, room) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        var body = {
            building: room.building,
            floor: room.floor,
            number: room.number
        };
        return APIService.put('/' + type, body, headers);
    }
}

export default new RoomsService();
