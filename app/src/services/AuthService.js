import APIService from "./APIService"
import CurrentUser from "../models/CurrentUser";

class AuthService {
    login(email, password) {
        var body = {
            email: email,
            password: password
        };

        return APIService.post('/signinAdmin', body).then(
            response => {
                CurrentUser.setUser(response.data);
            }
        );
    }

    logout() {
        CurrentUser.remove();
    }

    register(email, name, password, role) {
		var body = {
			email: email,
			displayName: name,
			password: password,
			role: role
		};
		var headers = {
			authorization: "Beaver " + CurrentUser.getToken(),
		};
		return APIService.post('/signUp', body, headers);
	}
}

export default new AuthService();
