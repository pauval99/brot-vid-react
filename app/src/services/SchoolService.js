import CurrentUser from "../models/CurrentUser";
import APIService from "./APIService";

class SchoolService {
    editSchool(type) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        return APIService.post('/'+ type + '/FIB', {}, headers);
    }

    getSchool() {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        return APIService.get('/getschool/FIB', headers);
    }
}

export default new SchoolService();
