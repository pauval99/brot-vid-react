import axios from "axios";

const API_URL = "http://35.180.26.95:3000/api";

class APIService {
    get(route, headers ) {
        return axios.get(API_URL + route, { headers: headers });
    }
    
    post(route, body, headers) {
        return axios.post(API_URL + route, body, { headers: headers });
    }

    put(route, body, headers) {
        return axios.put(API_URL + route, body, { headers: headers });
    }

    delete(route, headers) {
        return axios.delete(API_URL + route, { headers: headers });
    }
}

export default new APIService();
