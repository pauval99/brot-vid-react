import APIService from "./APIService";
import CurrentUser from "../models/CurrentUser";

class TwitterService {
    tweet(status) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        var body = { 
            status: status
        };
        return APIService.post('/tweet', body, headers);
    }
}

export default new TwitterService();
