import CurrentUser from "../models/CurrentUser";
import APIService from "./APIService";

class UsersService {
    getUsers() {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        return APIService.get('/users', headers);
    }

    getSeatHistory(user) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        return APIService.get('/seatHistory?email=' + user.email, headers);
    }

    getSeatHistoryBiblio(user) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        return APIService.get('/seatHistoryBiblio?email=' + user.email, headers);
    }

    getSubjects(user) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        return APIService.get('/subjectsUser/' + user._id, headers);
    }
}

export default new UsersService();
