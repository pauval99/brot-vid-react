import CurrentUser from "../models/CurrentUser";
import APIService from "./APIService";

class IncidenceService {
    resolveIncidence(incidence) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        return APIService.delete('/resolveIncidence/' + incidence._id, headers);
    }
}

export default new IncidenceService();
