import CurrentUser from "../models/CurrentUser";
import APIService from "./APIService";

class NotificationsService {
    getNotifications() {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        return APIService.get('/notifications', headers);
    }

    createNews(title, description) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        var body = {
            title: title,
            descripcion: description
        };
        return APIService.post('/addNews', body, headers);
    }

    createSecurityMeasure(title, description) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        var body = {
            title: title,
            descripcion: description
        };
        return APIService.post('/addSecurityMeasures', body, headers);
    }

    createRegulation(title, description) {
        var headers = {
            authorization: "Beaver " + CurrentUser.getToken(),
        };
        var body = {
            title: title,
            descripcion: description
        };
        return APIService.post('/addregulations', body, headers);
    }
}

export default new NotificationsService();
