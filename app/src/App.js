import React, { Component } from "react";
import { Switch, Route, Link, Redirect, NavLink } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "./App.css";

import AuthService from "./services/AuthService";
import CurrentUser from "./models/CurrentUser";

import Login from "./components/Login";
import Register from "./components/Register";
import Notifications from "./components/Notifications";
import CreateNotification from "./components/CreateNotification";
import Students from "./components/Students";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      logged: CurrentUser.isLogged(),
    };
  }

  logout() {
    this.setState({
      logged: false
    });

    AuthService.logout();
  }

  render() {
    const { logged } = this.state;

    return (
      <div>
        { logged &&
          <nav className="navbar navbar-expand sticky bg-darkBlue" >
            <div className="navbar-brand white">
              Brot-Vid
            </div>
            <div className="navbar-nav ml-auto">
              <NavLink to={"/notifications"} className="nav-link rounded white" activeClassName="active">
                Notificaciones
              </NavLink>
              <NavLink to={"/students"} className="nav-link rounded white" activeClassName="active">
                Alumnos
              </NavLink>
              <NavLink to={"/register"} className="nav-link rounded white" activeClassName="active">
                Registrar Usuario
              </NavLink>
              <li className="nav-item dropdown">
                <div className="clickable nav-link dropdown-toggle white" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  { CurrentUser.getDisplayName() }
                </div>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <Link to={"/"} onClick={() => this.logout()} className="dropdown-item">
                    Cerrar sessión
                  </Link>
                </div>
              </li>
            </div>
          </nav>
        }
        <Switch>
          <Route exact path="/">
            <Redirect to={ logged ? '/notifications' : '/login' } />
          </Route>
          <Route exact path="/login" component={Login} />
          <Route exact path="/notifications" component={Notifications} />
          <Route exact path="/notifications/create" component={CreateNotification} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/students" component={Students} />
        </Switch>
      </div>
    );
  }
}

export default App;
