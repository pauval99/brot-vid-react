const USER = 'user';

class CurrentUser {
    isLogged() {
        return localStorage.getItem(USER) != null;
    }

    setUser(user) {
        localStorage.setItem(USER, JSON.stringify({
            token: user.token,
            displayName: user.displayName,
            role: user.role
        }));
    }

    remove() {
        localStorage.removeItem(USER);
    }

    getToken() {
        return JSON.parse(localStorage.getItem(USER)).token;
    }

    getDisplayName() {
        return JSON.parse(localStorage.getItem(USER)).displayName;
    }

    getRole() {
        return JSON.parse(localStorage.getItem(USER)).role;
    }
}

export default new CurrentUser();
