class StringHelper {
    roomToString(room) {
        return room.building + room.floor + (room.number < 10 ? '0' + room.number : room.number);
    }
}

export default new StringHelper();
